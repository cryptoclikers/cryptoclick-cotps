﻿#VARIABLES
#RUTA DE LA BASE
$base = "C:\Cotp\Instaladores\Base v3.1.0\debug"
#RUTA DESTINO
$destino = "c:\cotp\clientes\"
#RUTA DESTINO
$ejecutable = "\debug\me.exe"
$servidor = $env:computername
#PROCESO
########################################
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$svc = New-WebServiceProxy –Uri ‘http://bitcode.com.co/servicios/sweb_biblioteca.asmx?WSDL’
$datos = $svc.ListCotp("%%")
[xml] $xdoc = $datos
foreach( $recurso in $xdoc.NewDataSet.Datos) 
{ 
    $comando = $destino +"\"+  $recurso.PLATAFORMA + "\"+ $recurso.NOMBRE
    $comando_ini = $destino +  $recurso.PLATAFORMA + "\"+ $recurso.NOMBRE + $ejecutable 
    IF (([int]$recurso.SEGUNDOS_INACTIVO -ge 120) -and ($recurso.ESTADO -eq "Activo")  -and ($recurso.SERVIDOR -eq $servidor)  ) {
        $comando = $destino +"\"+  $recurso.PLATAFORMA + "\"+ $recurso.NOMBRE
        Write-Host "Recuperado: "  $recurso.NOMBRE -ForegroundColor White  -BackgroundColor DarkRed 
        if (-not (Test-Path $comando))
        {
            New-Item $comando -itemType Directory
        }
        Copy-Item -Path $base -Destination $comando -recurse -Force
        START $comando_ini
    }

    IF (($recurso.ESTADO -eq "Nuevo") -and ($recurso.SERVIDOR -eq $servidor) ) {
        $comando = $destino +"\"+  $recurso.PLATAFORMA + "\"+ $recurso.NOMBRE
        Write-Host "Nuevo: "  $recurso.NOMBRE -ForegroundColor White  -BackgroundColor DarkRed 
        if (-not (Test-Path $comando))
        {
            New-Item $comando -itemType Directory
        }
        Copy-Item -Path $base -Destination $comando -Recurse -Force
        START $comando_ini
    }
}