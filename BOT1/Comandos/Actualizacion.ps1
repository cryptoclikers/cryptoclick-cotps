﻿$base = "C:\Cotp\Instaladores\Base v3.0.11\debug"
#RUTA DESTINO
$destino = "C:\Cotp\Clientes\"
#RUTA DESTINO
$ejecutable = "\debug\me.exe"
$servidor = $env:computername
#PROCESO
########################################
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$svc = New-WebServiceProxy –Uri ‘http://bitcode.com.co/servicios/sweb_biblioteca.asmx?WSDL’
$datos = $svc.ListCotp("%%")
[xml] $xdoc = $datos
foreach( $recurso in $xdoc.NewDataSet.Datos) 
{ 
    $comando = $destino +"\"+  $recurso.PLATAFORMA + "\"+ $recurso.NOMBRE
    $comando_ini = $destino +  $recurso.PLATAFORMA + "\"+ $recurso.NOMBRE + $ejecutable 
    IF (($recurso.ESTADO -eq "Actualizacion")  -and ($recurso.SERVIDOR -eq $servidor) ) {
        $comando = $destino +"\"+  $recurso.PLATAFORMA + "\"+ $recurso.NOMBRE
        Write-Host $comando_ini -ForegroundColor White  -BackgroundColor DarkRed 
        if (-not (Test-Path $comando))
        {
            New-Item $comando -itemType Directory
       
        }
        Copy-Item -Path $base -Destination $comando -recurse -Force
        START $comando_ini
    }
}



