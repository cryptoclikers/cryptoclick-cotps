﻿Add-Type -AssemblyName System.IO.Compression.FileSystem
# VARIABLES
#$servidor = "BOT1"
$servidor = $env:computername
#$destination = 'c:\Cotp-pruebas\'
$destination = 'c:\Cotp\'
  

$servidor = $servidor.ToUpper()
# PROGRAMA
$destination_zip = $destination + '\'+$servidor+'.zip'
$man_temo_dwn =  $destination +'cryptoclick-cotps-main-'+ $servidor +'\'+$servidor 
$man_temo_cli =  $destination +'cryptoclick-cotps-main-'+ $servidor +'\'+$servidor +'\Clientes' 
$man_temo_con =  $destination +'cryptoclick-cotps-main-'+ $servidor +'\'+$servidor +'\Comandos' 
$man_temo_ins =  $destination +'cryptoclick-cotps-main-'+ $servidor +'\'+$servidor +'\Instaladores' 

if (Test-Path $man_temo_dwn)
{
    Remove-Item -Path $man_temo_dwn  -Force -Recurse
}
function Unzip
{
    param([string]$zipfile, [string]$outpath)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}

# Source file location
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$source = 'https://gitlab.com/cryptoclikers/cryptoclick-cotps/-/archive/main/cryptoclick-cotps-main.zip?path=' + $servidor
#Download the file
#Invoke-WebRequest -Uri $source -OutFile $destination_zip
$client = new-object System.Net.WebClient
$client.DownloadFile($source,$destination_zip)

if (Test-Path $destination_zip)
    {
    Unzip $destination_zip $destination
    Remove-Item -Path $destination_zip -Force
    Copy-Item -Path $man_temo_cli -Destination $destination -recurse -Force
    Copy-Item -Path $man_temo_con -Destination $destination -recurse -Force
    Copy-Item -Path $man_temo_ins -Destination $destination -recurse -Force
    Remove-Item -Path $man_temo_dwn  -Force -Recurse
    if (Test-Path $man_temo_dwn)
    {
        Remove-Item -Path $man_temo_dwn  -Force -Recurse
    }
}

